import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Album from "./Album";
import {getAlbums} from "../../store/actions/albumActions";

const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  }
}));

const Albums = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const albums = useSelector(state => state.albums.albums);
  const loading = useSelector(state => state.albums.albumsLoading);
  const user = useSelector(state => state.users.user);


  useEffect(() => {
    dispatch(getAlbums(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  let artist = 'Anonymous';
  if (albums[0]) {
    artist = albums[0].artist.name;
  }


  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant='h4'>{artist}</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={1}>
        {loading ? (
          <Grid container justify='center' alignContent="center" className={classes.progress}>
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>) : albums.map(album => (
          <Album
            key={album._id}
            id={album._id}
            name={album.name}
            year={album.releaseDate}
            image={album.image}
            published={album.published}
            user={user}
          />
        ))}
      </Grid>
    </Grid>
  );
};

export default Albums;