import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Artist from "./Artist";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {getArtists} from "../../store/actions/artistActions";

const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  }
}));

const Artists = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const artists = useSelector(state => state.artists.artists);
  const loading = useSelector(state => state.artists.artistLoading);
  const user = useSelector(state => state.users.user);


  useEffect(() => {
    dispatch(getArtists());
  }, [dispatch]);

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant='h4'>Artists</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={1}>
        {loading ? (
          <Grid container justify='center' alignContent="center" className={classes.progress}>
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>) : artists.map(artist => (
          <Artist
            key={artist._id}
            id={artist._id}
            name={artist.name}
            description={artist.description}
            image={artist.image}
            published = {artist.published}
            user={user}
          />
        ))}
      </Grid>
    </Grid>
  );
};

export default Artists;