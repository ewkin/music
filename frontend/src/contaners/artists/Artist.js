import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";

import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {CardMedia} from "@material-ui/core";

import imageNotAvailable from '../../assets/images/notAvailable.png';
import {apiURL} from "../../config";
import DeleteIcon from '@material-ui/icons/Delete';
import PublishIcon from '@material-ui/icons/Publish'
import {useDispatch} from "react-redux";
import {deleteArtist, publishArtist} from "../../store/actions/artistActions";

const useStyles = makeStyles({
  card: {
    height: '100%'
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  }
});


const Artist = ({name, description, image, id, user, published}) => {
  const classes = useStyles();
  const dispatch = useDispatch();


  let cardImage = imageNotAvailable;
  if (image) {
    cardImage = apiURL + '/' + image;
  }

  return (
    <Grid item xs={12} sm={12} md={3} lg={3}>
      <Card className={classes.card}>
        <CardHeader title={name}/>
        <CardMedia
          image={cardImage}
          title={name}
          className={classes.media}
        />
        <CardContent>
          <strong style={{marginLeft: '10px'}}>
            Description: {description}
          </strong>
        </CardContent>
        <CardActions>
          <IconButton component={Link} to={'/albums/' + id}>
            <ArrowForwardIcon/>
          </IconButton>
          {user?.role === 'admin' && (
            <>
              <Grid item>
                <IconButton onClick={() => dispatch(deleteArtist(id, user._id))}
                            color="primary"><DeleteIcon/></IconButton>
              </Grid>
              {!published && (<Grid item>
                <IconButton onClick={()=>dispatch(publishArtist(id, user._id))} color="primary"><PublishIcon/></IconButton>
              </Grid>)}
            </>
          )}
        </CardActions>
      </Card>
    </Grid>
  );
};

Artist.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  image: PropTypes.string
};

export default Artist;