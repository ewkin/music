import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import FormElement from "../../components/UI/Form/FormElement";
import Button from "@material-ui/core/Button";
import FileInput from "../../components/UI/Form/FileInput";
import {getArtists} from "../../store/actions/artistActions";
import {addAlbum} from "../../store/actions/albumActions";

const AddAlbum = () => {

  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const artists = useSelector(state => state.artists.artists);


  useEffect(() => {
    dispatch(getArtists());
  }, [dispatch]);


  const [state, setState] = useState({
    name: '',
    releaseDate: '',
    artist: '',
    image: '',
  });

  const submitFormHandler = async e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    await dispatch(addAlbum(formData, user.token));
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.files[0];
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };


  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h4'>
          Add a new artist
        </Typography>
      </Grid>
      <Grid item xs>
        <form onSubmit={submitFormHandler}>
          <Grid container direction="column" spacing={2}>
            <FormElement
              required
              select
              label="Artist"
              name="artist"
              value={state.artist}
              onChange={inputChangeHandler}
              options={artists}
            />
            <FormElement
              required
              label="Name"
              name="name"
              value={state.name}
              onChange={inputChangeHandler}
            />

            <FormElement
              required
              label="Release Date"
              name="releaseDate"
              type="number"
              value={state.releaseDate}
              onChange={inputChangeHandler}
            />
            <Grid item xs>
              <FileInput
                name='image'
                label='Image'
                onChange={fileChangeHandler}
              />
            </Grid>
            <Grid item xs>
              <Button type="submit" color="primary" variant="contained">
                Add
              </Button>
            </Grid>
          </Grid>
        </form>

      </Grid>
    </Grid>
  );
};

export default AddAlbum;