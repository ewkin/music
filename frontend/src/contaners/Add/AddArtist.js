import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import FormElement from "../../components/UI/Form/FormElement";
import Button from "@material-ui/core/Button";
import FileInput from "../../components/UI/Form/FileInput";
import {addArtist} from "../../store/actions/artistActions";

const AddArtist = () => {

  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const [state, setState] = useState({
    name: '',
    description: '',
    image: '',
  });

  const submitFormHandler = async e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    await dispatch(addArtist(formData, user.token));
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.files[0];
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };


  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h4'>
          Add a new artist
        </Typography>
      </Grid>
      <Grid item xs>
        <form onSubmit={submitFormHandler}>
          <Grid container direction="column" spacing={2}>
            <FormElement
              required
              label="Name"
              name="name"
              value={state.name}
              onChange={inputChangeHandler}
            />
            <FormElement
              required
              multiline
              rows={3}
              label="Description"
              name="description"
              value={state.description}
              onChange={inputChangeHandler}
            />
            <Grid item xs>
              <FileInput
                name='image'
                label='Image'
                onChange={fileChangeHandler}
              />
            </Grid>
            <Grid item xs>
              <Button type="submit" color="primary" variant="contained">
                Add
              </Button>
            </Grid>
          </Grid>
        </form>

      </Grid>
    </Grid>
  );
};

export default AddArtist;