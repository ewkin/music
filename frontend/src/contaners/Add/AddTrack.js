import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import FormElement from "../../components/UI/Form/FormElement";
import Button from "@material-ui/core/Button";
import {getArtists} from "../../store/actions/artistActions";
import {getAlbums} from "../../store/actions/albumActions";
import {addTrack} from "../../store/actions/trackActions";

const AddTrack = () => {

  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const artists = useSelector(state => state.artists.artists);
  const albums = useSelector(state => state.albums.albums);


  useEffect(() => {
    dispatch(getArtists());
  }, [dispatch]);


  const [state, setState] = useState({
    name: '',
    length: '',
    number: '',
    video: '',
    album: ''
  });
  const [artist, setArtist] = useState('')


  const submitFormHandler = async e => {
    e.preventDefault();
    await dispatch(addTrack({...state}, user.token));
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const artistInputChangeHandler = async e => {
    dispatch(getAlbums(e.target.value));
    setArtist(e.target.value);
  };

  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h4'>
          Add a new artist
        </Typography>
      </Grid>
      <Grid item xs>
        <form onSubmit={submitFormHandler}>
          <Grid container direction="column" spacing={2}>
            <FormElement
              required
              select
              label="Artist"
              name="artist"
              value={artist}
              onChange={artistInputChangeHandler}
              options={artists}
            />
            {albums.length !== 0 && (
              <FormElement
                required
                select
                label="Album"
                name="album"
                value={state.album}
                onChange={inputChangeHandler}
                options={albums}
              />
            )}
            <FormElement
              required
              label="Name"
              name="name"
              value={state.name}
              onChange={inputChangeHandler}
            />
            <FormElement
              required
              label="Length"
              name="length"
              value={state.length}
              onChange={inputChangeHandler}
            />
            <FormElement
              required
              label="Number"
              name="number"
              type="number"
              value={state.number}
              onChange={inputChangeHandler}
            />
            <FormElement
              required
              label="Youtube video code"
              name="video"
              value={state.video}
              onChange={inputChangeHandler}
            />
            <Grid item xs>
              <Button type="submit" color="primary" variant="contained">
                Add
              </Button>
            </Grid>
          </Grid>
        </form>

      </Grid>
    </Grid>
  );
};

export default AddTrack;