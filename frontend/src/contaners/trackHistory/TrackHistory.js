import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {historyPush} from "../../store/actions/historyActions";
import {getTrackHistory, postTrackHistory} from "../../store/actions/trackHistoryAction";
import TrackHistoryItem from "./TrackHistoryItem";


const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  }
}));

const TrackHistory = () => {

  const classes = useStyles();
  const dispatch = useDispatch();
  const tracks = useSelector(state => state.trackHistory.trackHistory);
  const loading = useSelector(state => state.trackHistory.trackHistoryLoading);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    if (user) {
      dispatch(getTrackHistory(user.token));
    } else {
      dispatch(historyPush('/login'));
    }
  }, [dispatch, user]);


  const listenTrack = (id) => {
    dispatch(postTrackHistory({trackId: id, token: user.token}));
  }


  return (
    <Grid container direction="column" spacing={2}>

      <Grid item container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant='h4'>Track History</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={3}>
        {loading ? (
          <Grid container justify='center' alignContent="center" className={classes.progress}>
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>) : tracks.map(history => (
          <TrackHistoryItem
            key={history._id}
            id={history._id}
            name={history.track.name}
            artist={history.track.album.artist.name}
            time={history.datetime}
            image={history.track.album.image}
            listenTrack={() => listenTrack(history.track._id)}
            video={history.track.video}
          />
        ))}
      </Grid>
    </Grid>
  );
};

export default TrackHistory;