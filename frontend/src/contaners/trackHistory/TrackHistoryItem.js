import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import {CardMedia} from "@material-ui/core";
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import FavoriteIcon from '@material-ui/icons/Favorite';
import imageNotAvailable from "../../assets/images/notAvailable.png";
import {apiURL} from "../../config";
import ModalVideo from "react-modal-video";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    marginBottom: '10px'

  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  likeIcon: {
    height: 20,
    width: 20,
    color: 'red',
  },
}));


const TrackHistoryItem = ({name, artist, time, image, video, listenTrack}) => {
  const [isOpen, setOpen] = useState(false)

  const classes = useStyles();

  let cardImage = imageNotAvailable;
  if (image) {
    cardImage = apiURL + '/' + image;
  }
  let date = new Date(time);
  let datetime;
  if (time) {
    datetime = date.getDate() + "/"
      + (date.getMonth() + 1) + "/"
      + date.getFullYear() + " at "
      + date.getHours() + ":"
      + date.getMinutes() + ":"
      + date.getSeconds();
  }
  const play = () => {
    listenTrack();
    setOpen(true);
  }


  return (
    <Card className={classes.root}>
      <React.Fragment>
        <ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId={video} onClose={() => setOpen(false)}/>
      </React.Fragment>
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h5" variant="h5">
            {name}
          </Typography>
          <Typography component="h5" variant="h5">
            {artist}
          </Typography>
        </CardContent>
        <div className={classes.controls}>
          <IconButton onClick={play} aria-label="play/pause">
            <PlayArrowIcon className={classes.playIcon}/>
          </IconButton>
          <Typography variant="subtitle1" color="textSecondary">
            {datetime}
          </Typography>
          <IconButton aria-label="play/pause">
            <FavoriteIcon className={classes.likeIcon}/>
          </IconButton>
        </div>
      </div>
      <CardMedia
        className={classes.cover}
        image={cardImage}
        title="Album cover"
      />
    </Card>
  );
};

TrackHistoryItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  artist: PropTypes.string.isRequired,
  listenTrack: PropTypes.func.isRequired
};

export default TrackHistoryItem;
