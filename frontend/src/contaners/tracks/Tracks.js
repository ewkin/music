import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Track from "./Track";
import {historyPush} from "../../store/actions/historyActions";
import {postTrackHistory} from "../../store/actions/trackHistoryAction";
import {getTracks} from "../../store/actions/trackActions";

const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  }
}));

const Tracks = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const tracks = useSelector(state => state.tracks.tracks);
  const loading = useSelector(state => state.tracks.tracksLoading);
  const user = useSelector(state => state.users.user);


  useEffect(() => {
    dispatch(getTracks(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  let album = 'Anonymous';
  if (tracks[0]) {
    album = tracks[0].album.name;
  }
  const listenTrack = (id) => {
    if (user) {
      dispatch(postTrackHistory({trackId: id, token: user.token}));
    } else {
      dispatch(historyPush('/login'));
    }
  }


  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant='h4'>{album}</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={3}>
        {loading ? (
          <Grid container justify='center' alignContent="center" className={classes.progress}>
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>) : tracks.map(track => (
          <Track
            key={track._id}
            id={track._id}
            name={track.name}
            number={track.number}
            duration={track.length}
            image={track.album.image}
            video={track.video}
            published = {track.published}
            user={user}
            listenTrack={() => listenTrack(track._id)}
          />
        ))}
      </Grid>
    </Grid>
  );
};

export default Tracks;