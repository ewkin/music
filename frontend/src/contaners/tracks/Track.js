import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import {CardMedia} from "@material-ui/core";
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import FavoriteIcon from '@material-ui/icons/Favorite';
import imageNotAvailable from "../../assets/images/notAvailable.png";
import {apiURL} from "../../config";
import ModalVideo from "react-modal-video";
import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Delete";
import PublishIcon from "@material-ui/icons/Publish";
import {useDispatch} from "react-redux";
import {deleteTrack, publishTrack} from "../../store/actions/trackActions";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    marginBottom: '10px'

  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  likeIcon: {
    height: 20,
    width: 20,
    color: 'red',
  },
}));


const Track = ({name, number, id, duration, image, listenTrack, video, user, published}) => {
  const dispatch = useDispatch();

  const [isOpen, setOpen] = useState(false)

  const classes = useStyles();
  let cardImage = imageNotAvailable;
  if (image) {
    cardImage = apiURL + '/' + image;
  }

  const play = () => {
    listenTrack();
    setOpen(true);
  }


  return (
    <Card className={classes.root}>
      <React.Fragment>
        <ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId={video} onClose={() => setOpen(false)}/>
      </React.Fragment>
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h5" variant="h5">
            {number}. {name}
          </Typography>
        </CardContent>
        <div className={classes.controls}>
          <IconButton onClick={play} aria-label="play/pause">
            <PlayArrowIcon className={classes.playIcon}/>
          </IconButton>
          <Typography variant="subtitle1" color="textSecondary">
            {duration}
          </Typography>
          <IconButton aria-label="play/pause">
            <FavoriteIcon className={classes.likeIcon}/>
          </IconButton>
          {user?.role === 'admin' && (
            <>
              <Grid item>
                <IconButton onClick={() => dispatch(deleteTrack(id, user._id))}
                            color="primary"><DeleteIcon/></IconButton>
              </Grid>
              {!published && (<Grid item>
                <IconButton onClick={() => dispatch(publishTrack(id, user._id))}
                            color="primary"><PublishIcon/></IconButton>
              </Grid>)}
            </>
          )}
        </div>
      </div>
      <CardMedia
        className={classes.cover}
        image={cardImage}
        title="Album cover"
      />
    </Card>
  );
};

Track.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  duration: PropTypes.string.isRequired,
  listenTrack: PropTypes.func.isRequired
};

export default Track;
