import React from 'react';
import PropTypes from 'prop-types';
import {Grid, MenuItem, TextField} from "@material-ui/core";

const FormElement = ({
                       // required,
                       // name,
                       // label,
                       // value,
                       // onChange,
                       // type,
                       // autoComplete,
                       // multiline,
                       // rows
                       select,
                       options,
                       error,
                       ...props
                     }) => {
  let inputChildren = null;


  if (select) {
    inputChildren = options.map(option => (
      <MenuItem key={option._id} value={option._id}>
        {option.name}
      </MenuItem>
    ));
  }

  return (
    <Grid item xs>
      <TextField
        select={select}
        error={Boolean(error)}
        helperText={error}
        // required={required}
        // type={type}
        // label={label}
        // name={name}
        // autoComplete={autoComplete}
        // value={value}
        // onChange={onChange}
        {...props}
      >
        {inputChildren}
      </TextField>
    </Grid>
  );
};

FormElement.propTypes = {
  ...TextField.propTypes,
  options: PropTypes.arrayOf(PropTypes.object),
  error: PropTypes.string,
  select: PropTypes.bool
};

export default FormElement;