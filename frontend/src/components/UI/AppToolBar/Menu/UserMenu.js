import React, {useState} from 'react';
import {Menu, Button, MenuItem, Avatar} from "@material-ui/core";
import {historyPush} from "../../../../store/actions/historyActions";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";

const UserMenu = ({user}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const dispatch = useDispatch();


  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    dispatch(historyPush('/track_history'));
  };


  return (
    <>
      <Button color={'inherit'} onClick={handleClick}>
        <Avatar style={{marginRight: '10px'}} alt="User avatar" src={user.image}/>
        {user.displayName}
      </Button>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>Track history</MenuItem>
        <MenuItem onClick={() => {
          setAnchorEl(null);
          dispatch(historyPush('/add_artist'));
        }}>Add Artist</MenuItem>
        <MenuItem onClick={() => {
          setAnchorEl(null);
          dispatch(historyPush('/add_album'));
        }}>Add Album</MenuItem>
        <MenuItem onClick={() => {
          setAnchorEl(null);
          dispatch(historyPush('/add_track'));
        }}>Add Track</MenuItem>
        <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;