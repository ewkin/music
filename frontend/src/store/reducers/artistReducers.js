import {
  ADD_ARTIST_FAILURE,
  ADD_ARTIST_REQUEST,
  ADD_ARTIST_SUCCESS,
  GET_ARTIST_FAILURE,
  GET_ARTIST_REQUEST,
  GET_ARTIST_SUCCESS
} from "../actions/artistActions";

const initialState = {
  artists: [],
  artistLoading: false,
  artistError: null
};

const artistReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ARTIST_REQUEST:
      return {...state, artistLoading: true};
    case GET_ARTIST_SUCCESS:
      return {...state, artistLoading: false, artists: action.artists};
    case GET_ARTIST_FAILURE:
      return {...state, artistError: action.error, artistLoading: false};
    case ADD_ARTIST_REQUEST:
      return {...state, artistLoading: true};
    case ADD_ARTIST_SUCCESS:
      return {...state, artistLoading: false};
    case ADD_ARTIST_FAILURE:
      return {...state, artistError: action.error, artistLoading: false};
    default:
      return state;
  }
};
export default artistReducer;