import {
  ADD_HISTORY_FAILURE,
  ADD_HISTORY_REQUEST,
  ADD_HISTORY_SUCCESS,
  UPDATE_HISTORY_SUCCESS
} from "../actions/trackHistoryAction";

const initialState = {
  trackHistory: [],
  trackHistoryLoading: false,
  trackHistoryError: null
};

const trackHistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_HISTORY_REQUEST:
      return {...state, trackHistoryLoading: true};
    case ADD_HISTORY_SUCCESS:
      return {...state, trackHistoryLoading: false, trackHistory: action.tracks};
    case UPDATE_HISTORY_SUCCESS:
      return {...state, trackHistoryLoading: false};
    case ADD_HISTORY_FAILURE:
      return {...state, trackHistoryError: action.error, trackHistoryLoading: false};
    default:
      return state;
  }
};
export default trackHistoryReducer;