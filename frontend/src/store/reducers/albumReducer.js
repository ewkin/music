import {
  ADD_ALBUM_FAILURE,
  ADD_ALBUM_REQUEST,
  ADD_ALBUM_SUCCESS,
  GET_ALBUM_FAILURE,
  GET_ALBUM_REQUEST,
  GET_ALBUM_SUCCESS
} from "../actions/albumActions";


const initialState = {
  albums: [],
  albumsLoading: false,
  albumsError: null
};

const albumReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALBUM_REQUEST:
      return {...state, albumsLoading: true};
    case GET_ALBUM_SUCCESS:
      return {...state, albumsLoading: false, albums: action.albums};
    case GET_ALBUM_FAILURE:
      return {...state, albumsError: action.error, albumsLoading: false};
    case ADD_ALBUM_REQUEST:
      return {...state, artistLoading: true};
    case ADD_ALBUM_SUCCESS:
      return {...state, artistLoading: false};
    case ADD_ALBUM_FAILURE:
      return {...state, artistError: action.error, artistLoading: false};
    default:
      return state;
  }
};
export default albumReducer;