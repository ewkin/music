import {GET_TRACK_FAILURE, GET_TRACK_REQUEST, GET_TRACK_SUCCESS} from "../actions/trackActions";

const initialState = {
  tracks: [],
  tracksLoading: false,
  tracksError: null
};

const trackReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TRACK_REQUEST:
      return {...state, tracksLoading: true};
    case GET_TRACK_SUCCESS:
      return {...state, tracksLoading: false, tracks: action.tracks};
    case GET_TRACK_FAILURE:
      return {...state, tracksError: action.error, tracksLoading: false};
    default:
      return state;
  }
};
export default trackReducer;