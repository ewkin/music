import axiosApi from '../../axiosApi';
import {NotificationManager} from "react-notifications";
import {historyPush} from "./historyActions";

export const ADD_ALBUM_REQUEST = 'ADD_ALBUM_REQUEST';
export const ADD_ALBUM_SUCCESS = 'ADD_ALBUM_SUCCESS';
export const ADD_ALBUM_FAILURE = 'ADD_ALBUM_FAILURE';

export const GET_ALBUM_REQUEST = 'GET_ALBUM_REQUEST';
export const GET_ALBUM_SUCCESS = 'GET_ALBUM_SUCCESS';
export const GET_ALBUM_FAILURE = 'GET_ALBUM_FAILURE';

const addAlbumRequest = () => ({type: ADD_ALBUM_REQUEST});
const addAlbumSuccess = albums => ({type: ADD_ALBUM_SUCCESS, albums});
const addAlbumFailure = error => ({type: ADD_ALBUM_FAILURE, error});


const getAlbumRequest = () => ({type: GET_ALBUM_REQUEST});
const getAlbumSuccess = albums => ({type: GET_ALBUM_SUCCESS, albums});
const getAlbumFailure = error => ({type: GET_ALBUM_FAILURE, error});


export const getAlbums = id => {
  return async dispatch => {
    try {
      dispatch(getAlbumRequest());
      const response = await axiosApi.get('/albums/' + id);
      dispatch(getAlbumSuccess(response.data));
    } catch (error) {
      NotificationManager.error('Could not fetch albums')
      if (error.response && error.response.data) {
        dispatch(getAlbumFailure(error.response.data));
      } else {
        dispatch(getAlbumFailure({global: 'No internet'}));
      }
    }
  };
};

export const addAlbum = (albumData, token) => {
  return async dispatch => {
    try {
      dispatch(addAlbumRequest());
      console.log(albumData)
      const response = await axiosApi.post('/album', albumData, {headers: {'Authorization': token}});
      dispatch(addAlbumSuccess(response.data));
      dispatch(historyPush('/'));
      NotificationManager.success(albumData.name + ' was added');
    } catch (e) {
      dispatch(addAlbumFailure(e.response.data));
      NotificationManager.error(e.response.data.errors);
    }
  };
};

export const deleteAlbum = (id, token) => {
  return async dispatch => {
    try {
      dispatch(addAlbumRequest());
      await axiosApi.delete('/albums/' + id, {headers: {'Authorization': token}});
      dispatch(addAlbumSuccess({}));

      NotificationManager.success('Album was deleted');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(addAlbumFailure(e.response.data));
        NotificationManager.error(e.response.data.error);
      } else {
        dispatch(addAlbumFailure({global: 'No internet'}));
      }
    }
  }
};

export const publishAlbum = (id, token) => {
  return async dispatch => {
    try {
      dispatch(addAlbumRequest());
      const response = await axiosApi.put('/albums/' + id, {published: true}, {headers: {'Authorization': token}});
      dispatch(addAlbumSuccess(response.data));
      NotificationManager.success('Album was published');
    } catch (e) {
      dispatch(addAlbumFailure(e.response.data));
      NotificationManager.error(e.response.data.errors);
    }
  };
};