import axiosApi from "../../axiosApi";

export const ADD_HISTORY_REQUEST = 'ADD_HISTORY_REQUEST';
export const ADD_HISTORY_SUCCESS = 'ADD_HISTORY_SUCCESS';
export const ADD_HISTORY_FAILURE = 'ADD_HISTORY_FAILURE';

export const UPDATE_HISTORY_SUCCESS = 'UPDATE_HISTORY_SUCCESS';


const addHistoryRequest = () => ({type: ADD_HISTORY_REQUEST});
const addHistorySuccess = tracks => ({type: ADD_HISTORY_SUCCESS, tracks});
const updateHistorySuccess = track => ({type: UPDATE_HISTORY_SUCCESS, track});
const addHistoryFailure = error => ({type: ADD_HISTORY_FAILURE, error});


export const postTrackHistory = (data) => {
  return async dispatch => {
    try {
      dispatch(addHistoryRequest());
      const response = await axiosApi.post('/track_history', {"track": data.trackId}, {headers: {'Authorization': data.token}});
      dispatch(updateHistorySuccess(response.data));
      dispatch(getTrackHistory(data.token));
    } catch (error) {
      if (error.response && error.response.data) {
        dispatch(addHistoryFailure(error.response.data));
      } else {
        dispatch(addHistoryFailure({global: 'No internet'}));
      }
    }
  };
};

export const getTrackHistory = token => {
  return async dispatch => {
    try {
      dispatch(addHistoryRequest());
      // eslint-disable-next-line
      const response = await axiosApi.get('/track_history', {headers: {'Authorization': token}});
      dispatch(addHistorySuccess(response.data));
    } catch (error) {
      if (error.response && error.response.data) {
        dispatch(addHistoryFailure(error.response.data));
      } else {
        dispatch(addHistoryFailure({global: 'No internet'}));
      }
    }
  };
};