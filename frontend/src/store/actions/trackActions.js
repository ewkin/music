import axiosApi from '../../axiosApi';
import {NotificationManager} from "react-notifications";
import {historyPush} from "./historyActions";

export const ADD_TRACK_REQUEST = 'ADD_TRACK_REQUEST';
export const ADD_TRACK_SUCCESS = 'ADD_TRACK_SUCCESS';
export const ADD_TRACK_FAILURE = 'ADD_TRACK_FAILURE';

export const GET_TRACK_REQUEST = 'GET_TRACK_REQUEST';
export const GET_TRACK_SUCCESS = 'GET_TRACK_SUCCESS';
export const GET_TRACK_FAILURE = 'GET_TRACK_FAILURE';

const addTrackRequest = () => ({type: ADD_TRACK_REQUEST});
const addTrackSuccess = () => ({type: ADD_TRACK_SUCCESS});
const addTrackFailure = error => ({type: ADD_TRACK_FAILURE, error});


const getTracksRequest = () => ({type: GET_TRACK_REQUEST});
const getTracksSuccess = tracks => ({type: GET_TRACK_SUCCESS, tracks});
const getTracksFailure = error => ({type: GET_TRACK_FAILURE, error});


export const getTracks = id => {
  return async dispatch => {
    try {
      dispatch(getTracksRequest());
      const response = await axiosApi.get('/tracks/' + id);
      dispatch(getTracksSuccess(response.data));
    } catch (error) {
      NotificationManager.error('Could not fetch tracks')
      if (error.response && error.response.data) {
        dispatch(getTracksFailure(error.response.data));
      } else {
        dispatch(getTracksFailure({global: 'No internet'}));
      }
    }
  };
};

export const addTrack = (trackData, token) => {
  return async dispatch => {
    try {
      dispatch(addTrackRequest());
      const response = await axiosApi.post('/tracks', trackData, {headers: {'Authorization': token}});
      dispatch(addTrackSuccess(response.data));
      dispatch(historyPush('/'));
      NotificationManager.success(trackData.name + ' was added');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(addTrackFailure(e.response.data));
        NotificationManager.error(e.response.data.errors);
      } else {
        dispatch(addTrackFailure({global: 'No internet'}));
      }
    }
  };
};

export const deleteTrack = (id, token) => {
  return async dispatch => {
    try {
      dispatch(addTrackRequest());
      await axiosApi.delete('/tracks/' + id, {headers: {'Authorization': token}});
      dispatch(addTrackSuccess({}));
      NotificationManager.success('Track was deleted');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(addTrackFailure(e.response.data));
        NotificationManager.error(e.response.data.error);
      } else {
        dispatch(addTrackFailure({global: 'No internet'}));
      }
    }
  }
};

export const publishTrack = (id, token) => {
  return async dispatch => {
    try {
      dispatch(addTrackRequest());
      const response = await axiosApi.put('/tracks/' + id, {published: true}, {headers: {'Authorization': token}});
      dispatch(addTrackSuccess(response.data));
      NotificationManager.success('Track was published');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(addTrackFailure(e.response.data));

        NotificationManager.error(e.response.data.errors);
      } else {
        dispatch(addTrackFailure({global: 'No internet'}));
      }
    }
  };
};