import axiosApi from '../../axiosApi';
import {NotificationManager} from "react-notifications";
import {historyPush} from "./historyActions";

export const ADD_ARTIST_REQUEST = 'ADD_ARTIST_REQUEST';
export const ADD_ARTIST_SUCCESS = 'ADD_ARTIST_SUCCESS';
export const ADD_ARTIST_FAILURE = 'ADD_ARTIST_FAILURE';

export const GET_ARTIST_REQUEST = 'GET_ARTIST_REQUEST';
export const GET_ARTIST_SUCCESS = 'GET_ARTIST_SUCCESS';
export const GET_ARTIST_FAILURE = 'GET_ARTIST_FAILURE';

const addArtistRequest = () => ({type: ADD_ARTIST_REQUEST});
const addArtistSuccess = artists => ({type: ADD_ARTIST_SUCCESS, artists});
const addArtistFailure = error => ({type: ADD_ARTIST_FAILURE, error});


const getArtistRequest = () => ({type: GET_ARTIST_REQUEST});
const getArtistSuccess = artists => ({type: GET_ARTIST_SUCCESS, artists});
const getArtistFailure = error => ({type: GET_ARTIST_FAILURE, error});


export const getArtists = id => {
  return async dispatch => {
    try {
      dispatch(getArtistRequest());
      const response = await axiosApi.get('/artists');
      dispatch(getArtistSuccess(response.data));
    } catch (error) {
      NotificationManager.error('Could not fetch artists')
      if (error.response && error.response.data) {
        dispatch(getArtistFailure(error.response.data));
      } else {
        dispatch(getArtistFailure({global: 'No internet'}));
      }
    }
  };
};

export const addArtist = (artistData, token) => {
  return async dispatch => {
    try {
      dispatch(addArtistRequest());
      const response = await axiosApi.post('/artists', artistData, {headers: {'Authorization': token}});
      dispatch(addArtistSuccess(response.data));
      dispatch(historyPush('/'));
      NotificationManager.success(artistData.name + ' was added');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(addArtistFailure(e.response.data));
        NotificationManager.error(e.response.data.errors);
      } else {
        dispatch(addArtistFailure({global: 'No internet'}));
      }
    }
  };
};
export const deleteArtist = (id, token) => {
  return async dispatch => {
    try {
      dispatch(addArtistRequest());
      await axiosApi.delete('/artists/' + id, {headers: {'Authorization': token}});
      dispatch(addArtistSuccess({}));
      dispatch(getArtists());
      NotificationManager.success('Artist was deleted');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(addArtistFailure(e.response.data));
        NotificationManager.error(e.response.data.error);
      } else {
        dispatch(addArtistFailure({global: 'No internet'}));
      }
    }
  }
};

export const publishArtist = (id, token) => {
  return async dispatch => {
    try {
      dispatch(addArtistRequest());
      const response = await axiosApi.put('/artists/' + id, {published: true}, {headers: {'Authorization': token}});
      dispatch(addArtistSuccess(response.data));
      dispatch(historyPush('/'));
      dispatch(getArtists());

      NotificationManager.success('Artist was published');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(addArtistFailure(e.response.data));

        NotificationManager.error(e.response.data.errors);
      } else {
        dispatch(addArtistFailure({global: 'No internet'}));
      }
    }
  };
};