import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router} from 'react-router-dom';
import {NotificationContainer} from 'react-notifications';
import history from "./history";
import App from './App';
import {ThemeProvider} from "@material-ui/core";
import theme from "./theme";
import store from "./store/configureStore";

import 'react-notifications/lib/notifications.css';


const app = (
  <Provider store={store}>
    <Router history={history}>
      <ThemeProvider theme={theme}>
        <NotificationContainer/>
        <App/>
      </ThemeProvider>
    </Router>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
