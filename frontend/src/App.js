import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import "react-modal-video/scss/modal-video.scss";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import AppToolBar from "./components/UI/AppToolBar/AppToolBar";
import Artists from "./contaners/artists/Artists";
import Albums from "./contaners/albums/Albums";
import Tracks from "./contaners/tracks/Tracks";
import Register from "./contaners/Register/Register";
import Login from "./contaners/Login/Login";
import TrackHistory from "./contaners/trackHistory/TrackHistory";
import AddArtist from "./contaners/Add/AddArtist";
import {useSelector} from "react-redux";
import AddAlbum from "./contaners/Add/AddAlbum";
import AddTrack from "./contaners/Add/AddTrack";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props}/> : <Redirect to={redirectTo}/>;
};


const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <>
      <CssBaseline/>
      <header><AppToolBar/></header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Artists}/>
            <Route path="/albums/:id/"  component={Albums}/>
            <Route path="/tracks/:id/"  component={Tracks}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
            <ProtectedRoute
              isAllowed={user}
              redirectTo='/login'
              path="/track_history"
              component={TrackHistory}
            />

            <ProtectedRoute
              isAllowed={user}
              redirectTo='/login'
              exact
              path="/add_album"
              component={AddAlbum}
            />
            <ProtectedRoute
              isAllowed={user}
              redirectTo='/login'
              exact
              path="/add_track"
              component={AddTrack}
            />
            <ProtectedRoute
              isAllowed={user}
              redirectTo='/login'
              exact
              ppath="/add_artist"
              component={AddArtist}
            />

          </Switch>
        </Container>
      </main>
    </>
  );
};

export default App;
