const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: String,
  image: String,
  published: {
    type: Boolean,
    required: true,
    default: false,
  }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;