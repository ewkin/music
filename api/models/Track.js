const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  length: {
    type: String,
    required: true
  },
  number: {
    type: Number,
    required: true
  },
  video: {
    type: String,
  },
  album: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Album',
    required: true
  },
  published: {
    type: Boolean,
    required: true,
    default: false,
  }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;