const express = require('express');
const User = require('../models/User');
const path = require('path');
const config = require('../config');
const axios = require("axios");
const multer = require("multer");
const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
})

const upload = multer({storage});


const router = express.Router();

router.post('/', upload.single('image'), async (req, res) => {
  try {
    let image;
    if (req.file) {
      image = 'http://localhost:8000/uploads/' + req.file.filename;
    }

    const user = new User({
      email: req.body.email,
      password: req.body.password,
      displayName: req.body.displayName,
      image: image,
    });
    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({email: req.body.email});
  if (!user) {
    return res.status(400).send({error: 'Email or password is incorrect'});
  }
  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({error: 'Email or password is incorrect'});
  }
  user.generateToken();
  await user.save();
  return res.send({message: 'Email and password are correct', user});
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};
  if (!token) return res.send(success);
  const user = await User.findOne({token});
  if (!user) return res.send(success);
  user.generateToken();
  await user.save();
  return res.send(success);
});

router.get('/', async (req, res) => {
  try {
    const track = await User.find();

    if (track) {
      res.send(track);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/facebookLogin', async (req, res) => {
  const inputToken = req.body.accessToken;
  const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);

    if (response.data.data.error) {
      return res.status(401).send({message: 'Facebook token incorrect'});
    }

    if (response.data.data['user_id'] !== req.body.id) {
      return res.status(401).send({message: 'User id incorrect'});
    }

    let user = await User.findOne({email: req.body.email});

    if (!user) {
      user = await User.findOne({facebookId: req.body.id})
    }

    if (!user) {
      user = new User({
        email: req.body.email || nanoid(),
        password: nanoid(),
        facebookId: req.body.id,
        displayName: req.body.name,
        image: req.body.picture.data.url
      });
    }

    user.generateToken();
    await user.save();

    res.send({message: 'Success', user});
  } catch (e) {
    res.status(401).send({global: 'Facebook token incorrect'});
  }
});


module.exports = router;