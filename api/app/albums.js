const path = require('path');
const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');
const auth = require("../middleware/auth");
const User = require("../models/User");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath)
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const criteria = {};
    if (req.query.artist) {
      criteria.artist = req.query.artist;
    }
    const albums = await Album.find(criteria).populate('artist', 'name');
    if (albums) {
      res.send(albums);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {

    const token = req.get('Authorization');
    let albums = null;
    let user = null;
    if (token) {
      user = await User.findOne({token});
    }
    if (user && user.role === 'admin') {
      albums = await Album.find({artist: {_id: req.params.id}}).populate('artist', 'name').sort({releaseDate: 1});
      if (albums) {
        return res.send(albums);
      } else {
        return res.sendStatus(404);
      }
    }
    albums = await Album.find({
      artist: {_id: req.params.id},
      published: true
    }).populate('artist', 'name').sort({releaseDate: 1});

    if (albums) {
      return res.send(albums);
    } else {
      return res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const albumData = req.body;

    if (req.file) {
      albumData.image = 'uploads/' + req.file.filename;
    }
    const album = new Album(albumData);
    await album.save();
    res.send({album});
  } catch (e) {
    res.status(400).send(e);
  }

});


router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    await Album.findByIdAndDelete(req.params.id);
    res.send('Got a DELETE request at ' + req.params.id);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const album = await Album.findById(req.params.id);
    album.published = true;
    await album.save();
    res.send('Published ' + req.params.id);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
