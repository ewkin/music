const path = require('path');
const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const User = require("../models/User");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
})

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const token = req.get('Authorization');
    let user = null;
    if (token) {
      user = await User.findOne({token});
    }
    if (user && user.role === 'admin') {
      return res.send(await Artist.find());
    }
    const artists = await Artist.find({published: true});
    res.send(artists);

  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const artistData = req.body;
    if (req.file) {
      artistData.image = 'uploads/' + req.file.filename;
    }
    const artist = new Artist(artistData);
    await artist.save();
    res.send({artist})
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    await Artist.findByIdAndDelete(req.params.id);
    res.send('Got a DELETE request at ' + req.params.id);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const artist = await Artist.findById(req.params.id);
    artist.published = true;
    await artist.save();
    res.send('Published ' + req.params.id);
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;



