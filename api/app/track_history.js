const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const auth = require("../middleware/auth");

const router = express.Router();


router.post('/', async (req, res) => {
  const token = req.get('Authorization');
  if (!token) {
    return res.status(401).send({error: 'No token password'});
  }
  const user = await User.findOne({token});
  if (!user) {
    return res.status(401).send({error: 'Wrong token'});
  }

  const trackHistoryData = req.body;
  trackHistoryData.user = user._id;
  const trackHistory = new TrackHistory(trackHistoryData);
  try {
    await trackHistory.setDatetime();
    await trackHistory.save();
    res.send(trackHistory);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get('/', auth,  async (req, res) => {
  try {
    const trackHistory = await TrackHistory.find({user: user._id}).sort({datetime: -1}).populate({path: 'track', populate: {path: 'album', populate:{path: 'artist'}}});

    if (trackHistory) {
      res.send(trackHistory);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;