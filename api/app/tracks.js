const express = require('express');
const Track = require('../models/Track');
const auth = require("../middleware/auth");
const User = require("../models/User");
const permit = require("../middleware/permit");
const router = express.Router();


router.get('/', async (req, res) => {
  try {
    const criteria = {};
    let track = []
    if (req.query.album) {
      criteria.album = req.query.album;
    }
    if (req.query.artist) {
      const tracks = await Track.find().populate('album');
      tracks.map(oneTrack => {
        if (oneTrack.album.artist == req.query.artist) {
          track.push(oneTrack);
        }
      });
    } else {
      track = await Track.find(criteria).populate('album', 'name image');
    }
    if (track) {
      res.send(track);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const token = req.get('Authorization');
    let tracks = null;
    let user = null;
    if (token) {
      user = await User.findOne({token});
    }

    if (user && user.role === 'admin') {
      tracks = await Track.find({album: {_id: req.params.id}}).populate('album', 'name image').sort({number: 1});
      if (tracks) {
        return res.send(tracks);
      } else {
        return res.sendStatus(404);
      }
    }
    tracks = await Track.find({
      album: {_id: req.params.id},
      published: true
    }).populate('album', 'name image').sort({number: 1});

    if (tracks) {
      return res.send(tracks);
    } else {
      return res.sendStatus(404);
    }

  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, async (req, res) => {
  const trackData = req.body;
  const track = new Track(trackData);
  try {
    await track.save();
    res.send(track);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    await Track.findByIdAndDelete(req.params.id);
    res.send('Got a DELETE request at ' + req.params.id);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const track = await Track.findById(req.params.id);
    track.published = true;
    await track.save();
    res.send('Published ' + req.params.id);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;