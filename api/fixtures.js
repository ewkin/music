const mongoose = require('mongoose');
const config = require('./config');
const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);
  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [sum41, greenDay] = await Artist.create(
    {
      name: 'Sum 41',
      description: 'Канадская панк-рок-группа из города Эйджакс, Онтарио.',
      image: 'fixtures/Sum41.jpeg',
      published: true
    },
    {
      name: 'Green Day',
      description: 'Aмериканская панк-рок группа, основанная в 1986 году.',
      image: 'fixtures/GreenDay.jpg',
      published: false
    }
  );

  const [underclassHero, allKillerNoFiller, americanIdiot, fatherOfAllMotherfuckers] = await Album.create(
    {
      name: 'Underclass Hero',
      releaseDate: 2007,
      artist: sum41,
      image: 'fixtures/UnderclassHero.jpg',
      published: true
    },
    {
      name: 'All Killer No Filler',
      releaseDate: 2001,
      artist: sum41,
      image: 'fixtures/AllKillerNoFiller.jpg',
      published: true
    },
    {
      name: 'American Idiot',
      releaseDate: 2001,
      artist: greenDay,
      image: 'fixtures/AmericanIdiot.png',
      published: false
    },
    {
      name: 'Father of All Motherfuckers',
      releaseDate: 2020,
      artist: greenDay,
      image: 'fixtures/FatherofAllMotherfuckers.jpg',
      published: false
    },
  );

  await Track.create({
      name: 'Underclass Hero',
      length: '4:00',
      number: 1,
      video: 'PEz2d49XTk0',
      album: underclassHero,
      published: true
    },
    {
      name: 'Underclass Hero',
      length: '4:00',
      number: 2,
      video: 'PEz2d49XTk0',
      album: underclassHero,
      published: true
    },
    {
      name: 'Underclass Hero',
      length: '4:00',
      number: 3,
      video: 'PEz2d49XTk0',
      album: underclassHero,
      published: true
    },
    {
      name: 'Underclass Hero',
      length: '4:00',
      number: 4,
      video: 'PEz2d49XTk0',
      album: underclassHero,
      published: true
    },
    {
      name: 'Underclass Hero',
      length: '4:00',
      number: 5,
      video: 'PEz2d49XTk0',
      album: underclassHero,
      published: true
    },
    {
      name: 'Nothing on my back',
      length: '2:50',
      number: 1,
      video: 'v4X8pE5idII',
      album: allKillerNoFiller,
      published: true
    },
    {
      name: 'Nothing on my back',
      length: '2:50',
      number: 2,
      video: 'v4X8pE5idII',
      album: allKillerNoFiller,
      published: true
    },
    {
      name: 'Nothing on my back',
      length: '2:50',
      number: 3,
      video: 'v4X8pE5idII',
      album: allKillerNoFiller,
      published: true
    },
    {
      name: 'Nothing on my back',
      length: '2:50',
      number: 4,
      video: 'v4X8pE5idII',
      album: allKillerNoFiller,
      published: true
    },
    {
      name: 'Nothing on my back',
      length: '2:50',
      number: 5,
      video: 'v4X8pE5idII',
      album: allKillerNoFiller,
      published: true
    },
    {
      name: 'American Idiot',
      length: '2:59',
      number: 1,
      video: 'Ee_uujKuJMI',
      album: americanIdiot
    },
    {
      name: 'American Idiot',
      length: '2:59',
      number: 2,
      video: 'Ee_uujKuJMI',
      album: americanIdiot
    },
    {
      name: 'American Idiot',
      length: '2:59',
      number: 3,
      video: 'Ee_uujKuJMI',
      album: americanIdiot
    },
    {
      name: 'American Idiot',
      length: '2:59',
      number: 4,
      video: 'Ee_uujKuJMI',
      album: americanIdiot
    },
    {
      name: 'American Idiot',
      length: '2:59',
      number: 5,
      video: 'Ee_uujKuJMI',
      album: americanIdiot
    },
    {
      name: 'Father of All Motherfuckers',
      length: '2:31',
      number: 1,
      video: 'eXv00PJ9IQM',
      album: fatherOfAllMotherfuckers
    },
    {
      name: 'Father of All Motherfuckers',
      length: '2:31',
      number: 2,
      video: 'eXv00PJ9IQM',
      album: fatherOfAllMotherfuckers
    },
    {
      name: 'Father of All Motherfuckers',
      length: '2:31',
      number: 3,
      video: 'eXv00PJ9IQM',
      album: fatherOfAllMotherfuckers
    },
    {
      name: 'Father of All Motherfuckers',
      length: '2:31',
      number: 4,
      video: 'eXv00PJ9IQM',
      album: fatherOfAllMotherfuckers
    },
    {
      name: 'Father of All Motherfuckers',
      length: '2:31',
      number: 5,
      video: 'eXv00PJ9IQM',
      album: fatherOfAllMotherfuckers
    },
  );


  await User.create({
    email: 'user@music',
    displayName: 'John',
    password: '1qaz@WSX29',
    token: nanoid(),
    role: 'user'
  }, {
    email: 'admin@music',
    displayName: 'Mike',
    password: '1qaz@WSX29',
    token: nanoid(),
    role: 'admin'
  });

  await mongoose.connection.close();

};

run().catch(console.error);